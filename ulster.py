#!/usr/bin/python3

import requests
import json
from geojson import Point, LineString, Feature, FeatureCollection, dump
import re
import sys

#Set filename/paths for GeoJSON file output
stopsfile = "ulsterstops.geojson"
routesfile = "ulsterroutes.geojson"
#API URL for bus stops and routes
stopsurl = "https://api.peaktransit.com/v5/index.php?app_id=_RIDER&key=c620b8fe5fdbd6107da8c8381f4345b4&controller=stop2&action=list&agencyID=72"
routesurl = "https://api.peaktransit.com/v5/index.php?app_id=_RIDER&key=c620b8fe5fdbd6107da8c8381f4345b4&controller=shape2&action=list&agencyID=72"
#Simple function that takes a given URL and a JSON object name that contains a list of features. Makes the API request and returns the specified JSON object.
def jsonify ( url, objname ):
    jsonobj = json.loads(requests.post(url).content)
    return jsonobj[objname] 
stopsjson = jsonify(stopsurl, "stop")
routesjson = jsonify(routesurl, "shape")

#Simple function to save a file of specified name
def savegeojson ( filename, featurecollection):
    with open(filename, 'w') as fh:
        dump(featurecollection, fh)
    return

#Function to generate stops file
def stops ():
    #Initialize features list
    features = []

    #Loop through stops in request JSON and place them into geoJSON features
    for stop in stopsjson:
        #Create GeoJSON Point object...
        point = Point((stop["lng"],stop["lat"]))
        #...then append it to the list of features
        features.append(Feature(geometry=point, properties={"name": stop["longName"]}))

    #Make the GeoJSON Feature Collection
    featurecollection = FeatureCollection(features)

    #Save the stops file
    savegeojson(stopsfile, featurecollection)
    return

#Function to generate routes file
def routes ():
    #Initialize features list
    features = []

    #Loop through routes in request JSON and place them into geoJSON features
    for route in routesjson:
        #Create a list from the latlong values
        coordinates = re.split(";",route["points"])
        #Before we make the array 2D, we need to pop the last (empty) list item thanks to a trailing semicolon
        coordinates.pop()
        #Initialize an empty list for storing each segment
        segments = []
        #Loop through each lat-long coordinate
        for coordinate in coordinates:
            #Use the comma as the delimeter to split each coordinate item into 2 separate list items (one for lat, one for long)
            coordinate = re.split(",",coordinate)
            #These coordinates then each need to be converted to float values to satisfy the GeoJSON object
            coordinate = [float(i) for i in coordinate]
            #Append the split and converted lat long values to their 2-item list
            segments.append(coordinate)
        #Create GeoJSON LineString object, iterating through all the segments and making sure to place longitude [1] before latitude [0]...
        linestring = LineString([(i[1],i[0]) for i in segments])
        #...then append it to the list of features
        features.append(Feature(geometry=linestring, properties={"name": route["shapeName"], "routeID": route["routeID"]}))

    #Make the GeoJSON Feature Collection
    featurecollection = FeatureCollection(features)

    #Save the routes file
    savegeojson(routesfile, featurecollection)
    return

#Raise an error if there is more than one argument given
if len(sys.argv) != 2:
    raise ValueError('Please use "stops", "routes", or "both" as an argument.')
#If we have one argument, make sure it is within the 3 defined options
elif sys.argv[1] not in ("stops", "routes", "both"):
    raise ValueError('Please use "stops", "routes", or "both" as an argument.')
elif sys.argv[1] == "stops":
    stops()
    print("Generated " + stopsfile)
elif sys.argv[1] == "routes":
    routes()
    print("Generated " + routesfile)
elif sys.argv[1] == "both":
    stops()
    print("Generated " + stopsfile)
    routes()
    print("Generated " + routesfile)
