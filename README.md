# Ulster County UCAT Bus Maps

Extracts Ulster County Area Transit (UCAT) bus stop locations and routes from the UCAT Rider Portal website, and places them into GeoJSON format.

## Dependencies
* Python 3.x
    * Requests module for session functionality
    * JSON module for JSON-based geodata
    * GeoJSON module for writing proper GeoJSON
    * Regular expressions module for cleaning up messer-than-normal text
    * Sys module for processing command-line arguments
* Also of course depends on Peaktransit's [UCAT Rider Portal webpage](http://ucat.rider.peaktransit.com/).

## Usage
Invoke the script with a single argument: "stops", "routes", or "both" according to your needs.
